package ua.turskyi.realmexample.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Profile : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var name: String = ""
    var age: String = ""
    var language: String = ""
}
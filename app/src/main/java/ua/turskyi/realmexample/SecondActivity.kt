package ua.turskyi.realmexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_second.*
import ua.turskyi.realmexample.model.Profile

class SecondActivity : AppCompatActivity(R.layout.activity_second) {

    private var profile: Profile = Profile()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadData()

        btnSendToThirdActivity.setOnClickListener {
            insertOrUpdateProfile()
        }
    }

    private fun loadData() {
        val thread = Thread {
            val realm = Realm.getDefaultInstance()
            val profile = realm.where<Profile>(Profile::class.java).findFirst()

            if(profile != null){
                this.profile.id = profile.id
                this.profile.name = profile.name
                this.profile.age = profile.age
            }

        }

        thread.start()
    }

    private fun insertOrUpdateProfile() {
        val thread = Thread {
            val realm: Realm = Realm.getDefaultInstance()
            realm.beginTransaction()

            profile.language = editTextLanguage.text.toString()
            realm.insertOrUpdate(profile)

            realm.commitTransaction()
            realm.close()

            sendToThirdActivity()
        }

        thread.start()
    }

    private fun sendToThirdActivity() {
        val intent = Intent(this, ThirdActivity::class.java)
        this.startActivity(intent)
    }

}
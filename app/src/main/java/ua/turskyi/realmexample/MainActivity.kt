package ua.turskyi.realmexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import ua.turskyi.realmexample.model.Profile


class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        btnSendToSecondActivity.setOnClickListener {
            insertOrUpdateProfile()

//            insertAutoIncrementProfile()
//            getProfile()
//            sendToSecondActivity()
        }

        btnShowData.setOnClickListener {
            val intent = Intent(this, ThirdActivity::class.java)
            startActivity(intent)
        }
    }

 /* in case of creating list of objects */
//    private fun insertAutoIncrementProfile() {
//        val thread = Thread {
//            val realm: Realm = Realm.getDefaultInstance()
//            realm.beginTransaction()
//
//            val currentIdNum = realm.where<Profile>(Profile::class.java).max("id")
////            val profiles = realm.where<Profile>(Profile::class.java).findAll()
//
//            val index = if (currentIdNum != null) {
//                currentIdNum.toInt() + 1
//            } else {
//                1
//            }
//
//            val profile = realm.createObject(Profile::class.java, index)
////            val profile = realm.createObject(Profile::class.java)
////            profile.id = index
//            profile.name = editTextName.text.toString()
//            profile.age = editTextAge.text.toString()
//            realm.insertOrUpdate(profile)
//            realm.commitTransaction()
//            realm.close()
//
//            sendToSecondActivity()
//        }
//
//        thread.start()
//
//    }

    private fun insertOrUpdateProfile() {
        val thread = Thread {
            val realm: Realm = Realm.getDefaultInstance()
            realm.beginTransaction()

            val profile = Profile()
            profile.id = 1
            profile.name = editTextName.text.toString()
            profile.age = editTextAge.text.toString()

            realm.insertOrUpdate(profile)

            realm.commitTransaction()
            realm.close()

            sendToSecondActivity()
        }

        thread.start()
    }

    private fun sendToSecondActivity() {
        val intent = Intent(this, SecondActivity::class.java)
        this.startActivity(intent)
    }
}

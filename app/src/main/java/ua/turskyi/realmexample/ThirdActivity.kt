package ua.turskyi.realmexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_third.*
import ua.turskyi.realmexample.model.Profile

class ThirdActivity : AppCompatActivity(R.layout.activity_third) {

    private lateinit var profile: Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadData()
    }

    private fun loadData() {
        val thread = Thread {
            val realm = Realm.getDefaultInstance()
            val profile = realm.where<Profile>(Profile::class.java).findFirst()

            if(profile != null){
                this.profile = profile
                name.text = profile.name
                age.text = profile.age
                language.text = profile.language
            }
        }
        thread.start()
    }
}